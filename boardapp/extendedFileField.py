from django.db.models import FileField
from django.forms import forms
from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _

class ExtendedFileField(FileField):

	content_types = ""
	max_upload_size = ""

	def __init__(self, *args, **kwargs):
		try:
			self.content_types = kwargs.pop("content_types")
			self.max_upload_size = kwargs.pop("max_upload_size")
		except KeyError:
			pass
		super(ExtendedFileField, self).__init__(*args, **kwargs)

	def clean(self, *args, **kwargs):
		data = super(ExtendedFileField, self).clean(*args, **kwargs)

		file = data.file
		try:
			content_type = file.content_type
			if content_type in self.content_types:
				if file._size > self.max_upload_size:
					raise forms.ValidationError(
						_('File too big. Max allowed size: %s; current size: %s') 
						% (filesizeformat(self.max_upload_size), filesizeformat(file._size)))
			else:
				raise forms.ValidationError(_('Wrong filetype. Tried to add: %s. Allowed: %s') % ( content_type, self.content_types ))
		except AttributeError:
			pass
		return data;