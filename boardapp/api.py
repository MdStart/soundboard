from boardapp.models import Sound, Favorite, UserCode
from boardapp.responseHelper import ResponseHelper, ResponseCodes

def apiSearch(request):
    return ResponseHelper.getHttpResponse();

def apiFavorites(request):
    return ResponseHelper.getHttpResponse();

def apiRegister(request):
	#TODO: secure this, so that only mobile and webapps can create accounts
	try:
		userCode = UserCode()
		userCode.save()
		return ResponseHelper.getHTTPResponse({
				"userCode" : userCode.code
			})
	except Exception as e:
		return ResponseHelper.getHTTPErrorResponse(exception = e)
