# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import boardapp.extendedFileField


class Migration(migrations.Migration):

    dependencies = [
        ('boardapp', '0005_auto_20160521_2056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sound',
            name='file',
            field=boardapp.extendedFileField.ExtendedFileField(upload_to='sounds/'),
        ),
        migrations.AlterField(
            model_name='sound',
            name='icon',
            field=models.ImageField(upload_to='thumbs/'),
        ),
    ]
