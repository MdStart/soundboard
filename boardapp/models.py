from __future__ import unicode_literals

from django.db import models
from boardapp.extendedFileField import ExtendedFileField
from django.conf import settings

class Sound(models.Model):
    name = models.CharField(max_length=30, verbose_name="Name", default="mysound")
    file = ExtendedFileField(upload_to='sounds/', max_upload_size="20971520",
    	content_types=['audio/mp3', 'audio/mpeg', 'audio/wav', 'audio/ogg'])
    tags = models.CharField(max_length=255, verbose_name="Tags", blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Created At")
    icon = models.ImageField(upload_to='thumbs/')

    def __unicode__(self):
        return self.name;
        
    class Meta:
        verbose_name = "Sound"
        verbose_name_plural = "Sounds"

class UserCode(models.Model):
    code = models.CharField(max_length=6, verbose_name="Code", default='000000')

    def save(self, *args, **kwargs):
        super(UserCode, self).save(*args, **kwargs)
        if(self.code == '000000'):
            self.code = format(self.id, '06')
        else: return
        super(UserCode, self).save(*args, **kwargs)

    def __unicode__(self):
        return 'UserCode: ' + self.code;

    class Meta:
        verbose_name = "User Code"
        verbose_name_plural = "User Codes"

class Favorite(models.Model):
    sound = models.ForeignKey(Sound)
    userCode = models.ForeignKey(UserCode)

    def __unicode__(self):
        return 'User code: ' + self.userCode.__unicode__() + ' favorited: ' + self.sound.__unicode__();

    class Meta:
        verbose_name = "Favorite"
        verbose_name_plural = "Favorites"