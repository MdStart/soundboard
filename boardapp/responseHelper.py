from abc import ABCMeta
from django.http import HttpResponse
import json;

class ResponseCodes():
	SUCCESS = 0
	GENERIC_ERROR = 1
	NO_RESULTS = 2

class ResponseHelper():
	__metaclass__ = ABCMeta

	@staticmethod
	def getHTTPResponse(body = None):
		return HttpResponse(
        	json.dumps(ResponseHelper.createResponse(body)),
        	content_type="application/json"
        )

	@staticmethod
	def getHTTPErrorResponse(body = None, errorCode = ResponseCodes.GENERIC_ERROR, exception = None):
		return HttpResponse(
        	json.dumps(ResponseHelper.createErrorResponse(body, errorCode, exception)),
        	content_type="application/json"
        )

	@staticmethod
	def getHTTPNoResultsResponse(body = None):
		return HttpResponse(
        	json.dumps(ResponseHelper.createErrorResponse(body, ResponseCodes.NO_RESULTS)),
        	content_type="application/json"
        )

	@staticmethod
	def createResponse(body):
		responseData = {}
		responseData['status'] = ResponseCodes.SUCCESS
		responseData['data'] = body
		return responseData

	@staticmethod
	def createErrorResponse(body = None, errorCode = ResponseCodes.GENERIC_ERROR, exception = None):
		responseData = {}
		responseData['status'] = errorCode
		if body is None:
			responseData['data'] = {}
		else:
			responseData['data'] = body
		if exception is not None:
			responseData['exceptionMessage'] = str(exception)

		return responseData

	@staticmethod
	def createNoResultsResponse(body = None):
		return ResponseHelper.createErrorResponse(body = body, errorCode = ResponseCodes.NO_RESULTS)