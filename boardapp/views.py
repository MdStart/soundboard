from django.shortcuts import render
from django.http import HttpResponse
from boardapp.models import Sound
import json

def index(request):
    sounds = Sound.objects.all()
    return render(request, 'base.html', {
        'sounds':sounds,
    })

def searchQuery(request):
    if request.method == 'GET' and 'query' in request.GET:
        if request.GET.get('query') is None:
            return HttpResponse(
                json.dumps({"message: error"}),
                content_type="application/json"
            )
        response = {}
        data = []
        response["message"] = "success"
        sounds = Sound.objects.filter(tags__contains=request.GET.get('query'))
        if len(sounds) <= 0:
            response["message"] = "empty"
            sound = Sound.objects.all()
        for sound in sounds:
            soundData = {}
            soundData["name"] = sound.name
            soundData["file"] = sound.file.url
            soundData["icon"] = sound.icon.url
            data.append(soundData)
        response["data"] = data
        return HttpResponse(
            json.dumps(response),
            content_type="application/json"
        )
    else:
        return HttpResponse(
            json.dumps({"message: error"}),
            content_type="application/json"
        )