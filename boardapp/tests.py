from django.test import TestCase

# Create your tests here.

class RestApiTestCase(TestCase):

	userCodePass = "123456"
	userCodeFail = "000000"

	def setup(self):
		"""Setup test case:
		- create userCodePass
		- add favorites to userCodePass account"""
		pass

	def testRegisterUserCode(self):
		"""Create new user code for device"""
		pass


	def testImFeelingLucky(self):
		"""This should return random sound according to query.
		Returned sound should not repeat in a row."""
		pass

	def testSoundList(self):
		"""Load up all sounds with pagination."""
		pass

	def testFavoriteSoundList(self):
		"""Load up all favorited sounds - this test should load 
		sounds for userCodePass and return NO_RESULTS for 
		userCodeFail"""
		pass

	def testSoundSearch(self):
		"""Load up all queried sounds with pagination."""
		pass

	def testCMSPage(self):
		"""This should return content of a CMS page."""
		pass